"use strict"

/**
 Теоретичне питання:
 Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
 Механизм прототипного подражания создает у объекта потомка ссылку на свои публичные методы
 таким образом, они становятся доступны потомкам без копирования

 Для чого потрібно викликати super() у конструкторі класу-нащадка?
 Для вызова конструктора родительского класса

 Завдання:
 Створити клас Employee, у якому будуть такі характеристики - name (ім"я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об"єкта.
 Створіть гетери та сеттери для цих властивостей.
 Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
 Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
 Створіть кілька екземплярів об"єкта Programmer, виведіть їх у консоль.
 */

class Employee {

    constructor(name, age, salary) {
        this.name = name,
            this.salary = salary;
        this.age = age
    }

    getName() { return this.name; }
    getSal() { return this.salary; }
    getAge() { return this.age; }

    setName(value) { this.name = value; }
    setSal(value) { this.salary = value; }
    setAge(value) { this.age = value; }

}

class Programmer extends Employee {

    constructor(name, age, salary, lang) {
        super(name, age, salary),
            this.languages = lang;
    }

    getSal() { return this.salary * 3; }
}

const ignat = new Programmer("Ignat", 19, 5000, ["ru", "ua", "eng"])
const elza = new Employee("Elza", 32, 3300)
const andrey = new Programmer("Andrey", 26, 6099, ["ru", "ua", "eng"])
const nastya = new Employee("Nastya", 27, 3500)

console.log(ignat, andrey, nastya, elza);
console.log(andrey.getSal());
console.log(nastya.getSal());
console.log(elza.getSal());


